{
 ; This is defining a date format to 'map' desired structure for the file based on a date
 :receipt-path "'Bookeeping/Expenses/'yyyy'/Receipts/'MMMM"
 :date-format  "yyyy-MM-dd"
 :category-personal "PERSONAL"
 :spreadsheet-path "'Bookeeping/Expenses/'yyyy"
 :spreadsheet-name "yyyy' - Zafiro-Tech - expenses'"
 :expenses-sheet-name "Expenses"
 :expenses-date-format "yyyy-mm-dd"
 :expenses-sheet-interval "Expenses!A2:C"
 :url-date-format "yyyy-MM-dd"
 :csv-date-format "MM-dd-yyyy"}