(ns zaf.receipt-storage-svc.gdrive-storage-test
  (:require [clojure.test :refer :all]
            [zaf.receipt-storage-svc.gdrive-storage :refer :all]
            [clj-time.core :as t]
            [google-apps-clj.google-drive :as gdrive]))

(deftest test-get-path-line
  (testing "generate path based on date Sample #1"
    (let [path-line (get-receipt-path-line (t/date-time 2017 04 27))]
      (is (= path-line ["Bookeeping" "Expenses" "2017" "Receipts" "April"]))))

  (testing "generate path based on date Sample #2"
    (let [path-line (get-receipt-path-line (t/date-time 2018 12 06))]
      (is (= path-line ["Bookeeping" "Expenses" "2018" "Receipts" "December"])))))

(deftest test-get-search-query
  (testing "get search query test "
    (let [query (get-search-query "some-file-name")]
      (is (= query {:fields [:id :title], :query "title='some-file-name'", :model :files, :action :list})))))

(def get-child-info-redefs
  {#'gdrive/execute-query! (fn [cred query] (if (= query (get-search-query "root-folder")) [{:id "1234" :title "root-folder"}]))
   #'gdrive/list-files!    (fn [cred id configs] (if (= id "1234") [{:id "222" :title "folder222"} {:id "333" :title "folder333"}]))})

(deftest test-get-child-info
  (testing "get valid root element"
    (with-redefs-fn get-child-info-redefs
      #(is (= (get-child-info "dummy_cred" nil "root-folder") {:id "1234" :title "root-folder"}))))

  (testing "fail (returns nil) getting invalid root element"
    (with-redefs-fn get-child-info-redefs
      #(is (= (get-child-info "dummy_cred" nil "wrong-root-folder") nil))))

  (testing "get child for a valid parent"
    (with-redefs-fn get-child-info-redefs
      #(is (= (get-child-info "dummy_cred" "1234" "folder333") {:id "333" :title "folder333"}))))

  (testing "get non-existing child for a valid parent"
    (with-redefs-fn get-child-info-redefs
      #(is (= (get-child-info "dummy_cred" "1234" "folder555") nil)))))

(def get-current-location-redefs
  {#'get-child-info (fn [creds parent-id child-name]
                      (let [id-child-pair {:id parent-id :child child-name}]
                        (case id-child-pair
                          {:id nil :child "Bookeeping"} {:id "B1" :title "Bookeeping"}
                          {:id "B1" :child "Expenses"} {:id "E1" :title "Expenses"}
                          {:id "E1" :child "2017"} {:id "201" :title "2017"}
                          {:id "201" :child "Receipts"} {:id "R1" :title "Receipts"}
                          {:id "R1" :child "April"} {:id "A1" :title "April"}
                          nil)))})

(deftest test-get-current-receipt-location
  (testing "get existing location"
    (with-redefs-fn get-current-location-redefs
      #(is (= (get-current-receipt-location "some_cred" (t/date-time 2017 4 15)) {:id "A1" :title "April"})))))

(deftest test-get-current-spreadsheets-location
  (testing "get existing location"
    (with-redefs-fn get-current-location-redefs
      #(is (= (get-current-spreadsheet-location "some_cred" (t/date-time 2017 4 15)) {:id "201" :title "2017"})))))

(def upload-receipt-redefs
  {#'get-current-receipt-location (fn [creds j-date]
                                    (if (and (= creds "some_cred") (= j-date (t/date-time 2017 8 23)))
                                      {:id "L1" :title "some_dir"}))
   #'gdrive/upload-file!          (fn [& params]
                                    (if (= params ["some_cred", "L1" "content1" "title1"])
                                      {:created-date "some-date"}))})

(deftest test-upload-receipt
  (testing "upload recipient"
    (with-redefs-fn upload-receipt-redefs
      #(is (= (upload-receipt "some_cred" "content1" "title1" (t/date-time 2017 8 23)) true)))))