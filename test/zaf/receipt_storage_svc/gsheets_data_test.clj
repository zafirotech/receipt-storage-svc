(ns zaf.receipt-storage-svc.gsheets-data-test
  (:require [clojure.test :refer :all]
            [google-apps-clj.google-sheets-v4 :as gsheets]
            [zaf.receipt-storage-svc.gsheets-data :refer :all]
            [clj-time.core :as t]
            [google-apps-clj.google-drive :as gdrive])
  (:import (com.google.api.services.sheets.v4 Sheets)))

(def save-receipt-info-redefs
  {#'gsheets-svc (fn [] "dummy-svc")
   #'get-spreadsheet-id  (fn [spreadsheet-name] "spreadsheet-id")
   #'gsheets/find-sheet-id (fn [svc spreadsheet-id sheet-title] "sheet-id")
   #'gsheets/append-sheet    (fn [svc spreadsheet-id expenses-sheet-id rows]
                               (let [entry (first rows)
                                     date (get(get (first entry) "userEnteredValue") "numberValue")
                                     desc (get(get (second entry) "userEnteredValue") "stringValue")
                                     amount (get(get (nth entry 2) "userEnteredValue") "numberValue")]
                                 (str svc "|" spreadsheet-id "|" expenses-sheet-id "|" date "|" desc "|" amount)))})

(deftest test-save-receipt-info
  (testing "save receipt info"
    (with-redefs-fn save-receipt-info-redefs
        #(is (= (save-receipt-info (t/date-time 2017 07 31) "desc1" 35.57) "dummy-svc|spreadsheet-id|sheet-id|42947.0|desc1|35.57")))))


(def get-receipt-infos-redefs
  {#'gsheets-svc             (fn [] "dummy-svc")
   #'get-spreadsheet-id      (fn [spreadsheet-name] "spreadsheet-id")
   #'gsheets/get-cell-values (fn [^Sheets service spreadsheet-id sheet-ranges] [[[(t/date-time 2017 8 2) "on day 2" 222.22M] [(t/date-time 2017 8 3) "on day 3" 333.33M] [(t/date-time 2017 8 4) "on day 4" 444.44M]]])})


(deftest test-get-receipt-infos
  (testing "retrieve receipt list based on date interval (only 2 out of three should be pick up)"
    (with-redefs-fn get-receipt-infos-redefs
      #(is (= (get-receipt-infos (t/date-time 2017 8 3) (t/date-time 2017 8 4))) [[(t/date-time 2017 8 3) "on day 3" 333.33M] [(t/date-time 2017 8 4) "on day 4" 444.44M]] )))
  )
