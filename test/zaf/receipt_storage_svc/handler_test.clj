(ns zaf.receipt-storage-svc.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [zaf.receipt-storage-svc.handler :refer :all]
            [clojure.string :as s]
            [zaf.receipt-storage-svc.gdrive-storage :as gstorage]
            [clj-time.core :as t]
            [zaf.receipt-storage-svc.gsheets-data :as gsheets]))

(deftest test-app
  (testing "upload route"
    (let [response (app (mock/request :get "/receipt-storage-svc/upload"))]
      (is (= (:status response) 200))
      (is (= (:body response) (slurp (clojure.java.io/resource "public/upload.html"))))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/receipt-storage-svc/invalid"))]
      (is (= (:status response) 404)))))

(def upload-file-redefs
  {#'gstorage/upload-receipt (fn [& args] (s/join "-" args))})

(deftest test-upload-file
  (testing "upload with title"
    (with-redefs-fn upload-file-redefs
      #(is (= (upload-file "title1"
                           {:filename     "filename1.pdf",
                            :content-type "application/pdf",
                            :tempfile     "file_content",
                            :size         1000} "some_date") "file_content-title1.pdf-some_date"))))
  (testing "upload without title"
    (with-redefs-fn upload-file-redefs
      #(is (= (upload-file nil
                           {:filename     "filename1.pdf",
                            :content-type "application/pdf",
                            :tempfile     "file_content",
                            :size         1000} "some_date") "file_content-filename1.pdf-some_date")))))

(def process-upload-input-redef
  {#'upload-file (fn [title file j-date] (and (= title "t1") (= file {:size 10 :tempfile "f1"}) (= j-date (t/date-time 2018 03 02))))
   #'add-record  (fn [& args] true)})

(deftest test-process-upload-input
  (testing "process upload for personal with file"
    (with-redefs-fn process-upload-input-redef
      #(is (= (process-upload-input
                {"title" "t1" "description" "d1" "date" "2018-03-02" "amount" "30.15" "category" "PERSONAL" "file" {:size 10 :tempfile "f1"}})
              "FILEUPLOAD_OK-ADDRECORD_OK"))))

  (testing "process upload for business with file"
    (with-redefs-fn process-upload-input-redef
      #(is (= (process-upload-input
                {"title" "t1" "description" "d1" "date" "2018-03-02" "amount" "30.15" "category" "BUSINESS" "file" {:size 10 :tempfile "f1"}})
              "FILEUPLOAD_OK-ADDRECORD_NO"))))

  (testing "process upload for personal without file"
    (with-redefs-fn process-upload-input-redef
      #(is (= (process-upload-input
                {"title" "t1" "description" "d1" "date" "2018-03-02" "amount" "30.15" "category" "PERSONAL" "file" {:size 0 :tempfile nil}})
              "FILE_UPLOAD_NO-ADDRECORD_OK")))))

(deftest test-add-record
  (testing "add record to excel"
    (with-redefs-fn {#'gsheets/save-receipt-info (fn [j-date description amount] (str j-date "|" description "|" amount) )}
                    #(is (= (add-record (t/date-time 2017 8 1) "desc1" 123.15) "2017-08-01T00:00:00.000Z|desc1|123.15")))))