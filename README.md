# receipt-storage-svc

A minimal solution as a Service to keep track of receipts. It uses
google drive  to store receipts (images, pdfs, etc.) and google sheets to keep
track of entries used as personal expenses.

As the interaction with google realm it's only aiming a personal G storage spot, it's using two-legged OAuth authentication
with Google Auth. More info about it [here](https://developers.google.com/identity/protocols/OAuth2ServiceAccount).


## Svc Authentication

There is no authentication built-in so best to rely on mutual authentication over SSL for external uses.
[Nginx](https://nginx.org/en/) would be a good candidate for it. All funny SSL keys and certs handling would happen there.


## Prerequisites

You will need [Leiningen](https://github.com/technomancy/leiningen) 2.0.0 or above installed.


## Running

To start a web server for the application, run:

    lein ring server


## Related projects

Although the application itself it's providing a simple HTML page to upload receipt information,
there are couple of projects to make this more interesting:

+ [receipt-uploader](https://bitbucket.org/zafirotech/receipt-uploader) An android app to upload receipts from the phone using this service
+ [receipt-storage-batch](https://bitbucket.org/zafirotech/receipt-storage-batch) A batch solution (built in python) to upload receipts using this service


## License

GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

