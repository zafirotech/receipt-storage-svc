(ns zaf.receipt-storage-svc.gsheets-data
  (:require [google-apps-clj.google-sheets-v4 :as gsheets]
            [google-apps-clj.credentials :as gcred]
            [google-apps-clj.google-drive :as gdrive]
            [zaf.receipt-storage-svc.gdrive-storage :as gdrive-storage]
            [zaf.receipt-storage-svc.configuration :refer [conf]]
            [clj-time.core :as t]
            [clj-time.format :as f]))

(defn gsheets-creds []
  (let [scopes [com.google.api.services.sheets.v4.SheetsScopes/SPREADSHEETS]]
    (gcred/default-credential scopes)))


(defn gsheets-svc []  (gsheets/build-service (gsheets-creds)))


(defn get-spreadsheet-name [j-date]
  (let [spreadsheet-formater (f/formatter (conf :spreadsheet-name))]
    (f/unparse spreadsheet-formater j-date)))

(defn get-spreadsheet-id [spreadsheet-name]
  (:id (first (gdrive/execute-query! (gdrive-storage/gdrive-creds) (gdrive-storage/get-search-query spreadsheet-name)))))

(defn get-expenses-sheet-id [svc spreadsheet-id]
    (gsheets/find-sheet-id svc spreadsheet-id (conf :expenses-sheet-name)))

(defn save-receipt-info
  "Save a receipt entry in the google spreadsheet associated (by year usually) with the date"
  [j-date description amount]
  (let [svc (gsheets-svc)
        spreadsheet-name (get-spreadsheet-name j-date)
        spreadsheet-id (get-spreadsheet-id spreadsheet-name)
        expenses-sheet-id (get-expenses-sheet-id svc spreadsheet-id)]
    (gsheets/append-sheet svc spreadsheet-id expenses-sheet-id [[(gsheets/date-cell (conf :expenses-date-format) j-date) (gsheets/coerce-to-cell description) (gsheets/currency-cell amount)]])))


(defn get-receipt-infos
  "Retrieve all the entries in 'expenses' sheet within a date interval"
  [j-start-date j-end-date]
  (let [svc (gsheets-svc)
        spreadsheet-name (get-spreadsheet-name j-start-date)
        spreadsheet-id (get-spreadsheet-id spreadsheet-name)
        cell-values (first(gsheets/get-cell-values svc spreadsheet-id [(conf :expenses-sheet-interval)]))]
    (filter
      (fn [row]
        (let [j-date (first row)]
          (and j-date (not (or (t/before? j-date j-start-date) (t/after? j-date j-end-date))))))
      cell-values)
    ))


