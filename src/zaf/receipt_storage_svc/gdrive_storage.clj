(ns zaf.receipt-storage-svc.gdrive-storage
  (:require [google-apps-clj.google-drive :as gdrive]
            [google-apps-clj.credentials :as gcred]
            [zaf.receipt-storage-svc.configuration :refer [conf]]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.string :as str]))


(defn get-path-line
  "Return path line merging config path and date"
  [path j-date]
  (let [path-formater (f/formatter path)
        path (f/unparse path-formater j-date)]
    (str/split path #"/")))

(defn get-receipt-path-line
  "Return path line for receipts"
  [j-date]
  (get-path-line (conf :receipt-path) j-date))

(defn get-spreadsheet-path-line
  "Return path line for receipts"
  [j-date]
  (get-path-line (conf :spreadsheet-path) j-date))

(defn gdrive-creds []
  (let [scopes [com.google.api.services.drive.DriveScopes/DRIVE]]
    (gcred/default-credential scopes)))

(defn get-search-query
  [name]
  (gdrive/file-list-query {:fields [:id :title] :query (str "title='" name "'")}))

(defn get-by-name
  "Return one element with the filename. It select first one so better be unique"
  [creds filename]
  (first (gdrive/execute-query! creds (get-search-query filename))))

(defn get-child-info
  "Retrieve the gdrive child info [id, name] based on the parent.
  If parent not provided does a query search based only on name only"
  [creds parent-id child-name]
  (if parent-id
    (first (filter (fn [entry] (= child-name (:title entry)))
                   (gdrive/list-files! creds parent-id {:fields [:id :title]})))
    (get-by-name creds child-name)))

(defn get-current-location
  "Get current location {:id :title} based on path line function"
  [creds j-date path-line-fn]
  (let [path-line (path-line-fn j-date)]
    (reduce
      (fn
        [current-dir-info next-dir-name]
        (let [next-dir-info (get-child-info creds (:id current-dir-info) next-dir-name)]
          (if next-dir-info
            next-dir-info
            (do
              (gdrive/create-folder! creds (:id current-dir-info) next-dir-name)
              (get-child-info creds (:id current-dir-info) next-dir-name)))))
      {:id nil :title nil} path-line)))

(defn get-current-receipt-location
  "Get current receipt location {:id :title}"
  [creds j-date]
  (get-current-location creds j-date get-receipt-path-line))

(defn get-current-spreadsheet-location
  "Get current spreadsheet location {:id :title}"
  [creds j-date]
  (get-current-location creds j-date get-spreadsheet-path-line))

(defn upload-receipt
  "Upload file to receipt location based on date"
  ([creds content file-title j-date]
   (let [location-id (:id (get-current-receipt-location creds j-date))]
     (contains? (gdrive/upload-file! creds location-id content file-title) :created-date)))
  ([content file-title j-date]
   (upload-receipt (gdrive-creds) content file-title j-date)))