(ns zaf.receipt-storage-svc.configuration
  (:require [clojure.java.io :as io]))


;; TODO use diferent config path for test env to allow changes in main config without affecting the tests
(def config (read-string (slurp (io/resource "config.clj"))))

(defn conf
  "Return a configuration value based on keyword"
  [keyword]
  (get config keyword))
