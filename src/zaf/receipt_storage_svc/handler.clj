(ns zaf.receipt-storage-svc.handler
  (:require [compojure.core :refer :all]
            [ring.middleware.multipart-params :as mp]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [clj-time.format :as f]
            [clj-time.core :as t]
            [clojure.string :as s]
            [zaf.receipt-storage-svc.configuration :refer [conf]]
            [zaf.receipt-storage-svc.gdrive-storage :as gstorage]
            [zaf.receipt-storage-svc.gsheets-data :as gsheets]
            [ring.util.response :refer [response header content-type]]
            [ring.adapter.jetty :only [run-jetty]]))

(def date-formater (f/formatter (conf :date-format)))

(defn upload-file
  "Upload file from multipart"
  [title file j-date]
  (let [{content :tempfile u-filename :filename} file
        g-filename (if title (str title "." (last (s/split u-filename #"\."))) u-filename)]
    (gstorage/upload-receipt content g-filename j-date)))

(defn add-record
  "Add record into spreadsheet"
  [j-date description amount]
    (gsheets/save-receipt-info j-date description amount))

(defn normalize-keys
  "Remove colon character from key"
  [dict]
  (reduce-kv  (fn [m k v]
                (let [nk (if (keyword? k ) (name k)  k)]
                  (assoc m nk v))) {} dict))

(defn process-upload-input [params]
  (let [params-rn (normalize-keys params)
        {title  "title" description "description" str-date "date"
         amount "amount" category "category" file "file"} params-rn
        j-date (if (empty? str-date) (t/now) (f/parse date-formater str-date))]
    (do
      (s/join "-" [(if (and (some? file) (> (:size file) 0))
                     (if (upload-file title file j-date) "FILEUPLOAD_OK" "FILEUPLOAD_FAIL") "FILE_UPLOAD_NO")
                   (if (= (conf :category-personal) (s/upper-case category))
                     (if (add-record j-date description (. Double parseDouble amount)) "ADDRECORD_OK" "ADDRECORD_FAIL") "ADDRECORD_NO")]))))

(defn str-to-date [str-date]
  (let [formatter (f/formatter (conf :url-date-format))]
    (f/parse formatter str-date)))

(defn data-to-csv-row [data]
  (let [formatter (f/formatter (conf :csv-date-format))
        csv-date (f/unparse formatter (first data))
        desc (second data)
        amount (* -1 (last data))]
    (str csv-date "," desc "," amount "\n")))

(defn generate-csv [start-date end-date]
  (let [data-list (gsheets/get-receipt-infos (str-to-date start-date) (str-to-date end-date))
        body (reduce (fn [csv-content data] (str csv-content (data-to-csv-row data))) "Date,Description,Amount\n" data-list)]
    (-> body
        response
        (content-type "text/csv")
        (header "Content-Disposition" (format "attachment; filename=\"expenses_%s_%s.csv\"" start-date end-date)))))

(defroutes app-routes
           (GET "/receipt-storage-svc/upload" [] (slurp (clojure.java.io/resource "public/upload.html")))
           (mp/wrap-multipart-params
             (POST "/receipt-storage-svc/store" {params :params} (process-upload-input params)))
           (GET "/receipt-storage-svc/csv" [start-date end-date] (generate-csv start-date end-date))

           (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes api-defaults))


(defn start
  "Just to start a server locally for testing purposes only"
  [] (ring.adapter.jetty/run-jetty #'app {:join? false :port 3000}))
